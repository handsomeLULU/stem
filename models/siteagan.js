const mongoose = require('mongoose');
/*
mongoose.connect(
  'mongodb://db:27017/agan',
  { useNewUrlParser: true },
);
*/
conn=mongoose.createConnection('mongodb://db:27017/agan');
//const service = require('../config/service');
//mongoose.connect(service.db.url, service.db.options)
// create new param schema
const ParamSchema = mongoose.Schema({
  // title: { type: String, required: true, unique: true },
  //id_num: { type: [Number], required: true, default: [0, 99999] },
  humidity: { type: [Number], required: true, default: [50, 70] },
  temperature: { type: [Number], required: true, default: [24, 30] },
  soil_moisture1: { type: [Number], required: true, default: [60, 80] },
  soil_moisture2: { type: [Number], required: true, default: [60, 80] },
  soil_moisture3: { type: [Number], required: true, default: [60, 80] },
  soil_moisture4: { type: [Number], required: true, default: [60, 80] },
  soil_temperature: { type: [Number], required: true, default: [20, 27] },
  luminance: { type: [Number], required: true, default: [400, 700] },
  ph: { type: [Number], required: true, default: [6.5, 7.5] },
  ec: { type: [Number], required: true, default: [3, 4] },
});

const SiteSchema = mongoose.Schema({
  // mongoose automatically checks 
  location: { type: [Number], required: true },
  landNumber: { type: String, required: true },
  area: { type: Number, required: true },
  plantation: { type: String, required: true },
  owner: { type: mongoose.Schema.Types.ObjectId, required: true },
  params: { type: ParamSchema, default: ParamSchema },
  sensorId: { type: Number, required: true },
  water: { type: Boolean, default: false },
  lights: { type: Boolean, default: false },
  fan: { type: Boolean, default: false },
});

// add pre-save function to check location duplicacy in the same land number?

module.exports = conn.model( 'Site', SiteSchema);
//module.exports = mongoose.model('Sensor', SiteSchema);
