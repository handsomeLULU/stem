const mongoose = require('mongoose');

/*mongoose.connect(
  'mongodb://db:27017/karotte',
  { useNewUrlParser: true },
);
*/
const bcrypt = require('bcryptjs');
const UserSchema = mongoose.Schema({
  name: { type: String, required: true },
  email: { type: String, required: true },
  username: { type: String, unique: true, required: true },
  password: { type: String, required: true },
  role: { type: String, default: 'ordinary' },
  site: [mongoose.Schema.Types.ObjectId],
  // config: [ mongoose.Schema.Types.ObjectId ]
});

// middleware, automatically runs this function before save
UserSchema.pre('save', async function () {
  let user = this;
  if (this.isModified('password') || this.isNew) {
    try {
      let salt = await bcrypt.genSalt(10);
      let hash = await bcrypt.hash(user.password, salt);
      // substitute password w/ hash
      user.password = hash
    } catch (e) {
      console.log(e);
    }
  }
});

// init toObject options if not exist
if (!UserSchema.options.toObject) UserSchema.options.toObject = {};
// define a option for toObject called `hide`
// which in default only hides the password
UserSchema.options.toObject.hide = 'password';
UserSchema.options.toObject.transform = function (doc, ret, options) {
  if (options.hide) {
    options.hide.split(' ').forEach(function (prop) {
      delete ret[prop];
    });
  }
}

UserSchema.methods.comparePassword = function (passwd) {
  const savedPassword = this.password;
  return new Promise(async function (resolve, reject) {
    try {
      const isMatch = await bcrypt.compare(passwd, savedPassword);
      resolve(isMatch)
    } catch (e) {
      console.log(e);
      return reject(e);
    }
  });
}

module.exports = mongoose.model('User', UserSchema);
