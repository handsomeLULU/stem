const mongoose = require('mongoose');

const announcementSchema = mongoose.Schema({
  title: { type: String, required: true },
  body: { type: String, required: true },
  date: { type: Date, default: Date.now }
});

// default collection name is all lower case with an 's'
// i.e. model name: Announcement -> collection name: announcements
module.exports = mongoose.model('Announcement', announcementSchema);