const mongoose = require('mongoose');

/*
mongoose.connect(
  'mongodb://db:27017/karotte',
  { useNewUrlParser: true },
);
*/
conn=mongoose.createConnection('mongodb://db:27017/error');
//const service = require('../config/service');
//mongoose.connect(service.db.url, service.db.options)
// create new param schema
const ParamSchema = mongoose.Schema({
  // title: { type: String, required: true, unique: true },
  //id_num: { type: [Number], required: true, default: [0, 99999] },
  humidity: { type: [Number], required: true, default: [50, 70] },
  temperature: { type: [Number], required: true, default: [24, 30] },
  soil_moisture1: { type: [Number], required: true, default: [60, 80] },
  soil_moisture2: { type: [Number], required: true, default: [60, 80] },
  soil_moisture3: { type: [Number], required: true, default: [60, 80] },
  soil_moisture4: { type: [Number], required: true, default: [60, 80] },
  soil_temperature: { type: [Number], required: true, default: [20, 27] },
  luminance: { type: [Number], required: true, default: [400, 700] },
  ph: { type: [Number], required: true, default: [6.5, 7.5] },
  ec: { type: [Number], required: true, default: [3, 4] },
});

const SiteSchema = mongoose.Schema({
  // mongoose automatically checks 
  number: { type: Number, required: true },
  name: [String],
  id: [String],
  site: { type: Array, required: true },

});
const errorSchema = mongoose.Schema({
  // mongoose automatically checks 
  number:{ type: String, required: true },
  time: { type: String, required: true },
  data:{ type: String, required: true }
  
});

// add pre-save function to check location duplicacy in the same land number?

module.exports = conn.model('aganerrorlog', errorSchema);
//module.exports = mongoose.model('Sensor', SiteSchema);
