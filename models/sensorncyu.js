const mongoose = require('mongoose');
/*
mongoose.connect(
  'mongodb://db:27017/agan',
  { useNewUrlParser: true },
);
*/
conn=mongoose.createConnection('mongodb://db:27017/ncyu');
// find date
// const date = new Date();
// const year = date.getFullYear();
// // getMonth returns 0-11, change it to human readable 1-12
// const month = date.getMonth() + 1;
// const daysInMonth = new Date(year, month, 0).getDate();

// function arrayLimit(arr) {
//   return arr.length <= 24;
// }

const SensorSchema = mongoose.Schema({
  _id: { type: String, required: true },
  sensors: { type: Map },
  count: { type: Map },
  total: { type: Map },
});

// const SensorSchema = mongoose.Schema({
//   _id: { type: String, required: true },
//   humidity: [[Number]],
//   temperature: [[Number]],
//   soil_moisture: [[Number]],
//   soil_temperature: [[Number]],
//   luminance: [[Number]],
//   PH: [[Number]],
//   EC: [[Number]]
// });

// SensorSchema.pre('save', async function () {
//   // change the id to special form
//   // const sensor = this;
//   // sensor._id = `${this._id}:${year}${(month < 10 ? '0' : '') + month}`;
//   // pre-allocated length of mongth
//   sensor.humidity.push([]);
//   sensor.temperature.push([]);
//   sensor.soil_moisture.push([]);
//   sensor.soil_temperature.push([]);
//   sensor.luminance.push([]);
//   sensor.PH.push([]);
//   sensor.EC.push([]);
// });

module.exports = conn.model('Sensor', SensorSchema);
