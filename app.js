const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
//const linebot = require('linebot');
const cors = require('cors');
const mongoose = require('mongoose');
const mongooseagan = require('mongoose');
const mqtt = require('async-mqtt');
var errors = [[[], []], [[], []], [[], []], [[], []]];
const service = require('./config/service');
const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');
const authRouter = require('./routes/auth');
const sitesRouter = require('./routes/sites');
const Sensor = require('./models/sensor');
const aSensor = require('./models/sensoragan');
const ncyuSensor = require('./models/sensorncyu');
var engine = require('ejs-locals');
const app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(cors());
// setting routes
app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/auth', authRouter);
app.use('/sites', sitesRouter);

// create new mqtt client
const client = mqtt.connect(service.mqttOpts);
const client2 = mqtt.connect(service.mqttOpts);
const clientagan = mqtt.connect(service.mqttOpts);
const clientncyu = mqtt.connect(service.mqttOpts);
clientagan.once('connect', async () => {
  try {
    await clientagan.subscribe('agan/sensors/+');
  } catch (e) {
    console.log(e);
  }
});
clientncyu.once('connect', async () => {
  try {
    await clientncyu.subscribe('ncyu/sensors/+');
  } catch (e) {
    console.log(e);
  }
});
client.once('connect', async () => {
  try {
    await client.subscribe('karotte/sensors/+');
  } catch (e) {
    console.log(e);
  }
});
client2.once('connect', async () => {
  try {
    await client2.subscribe('karotte/errors/+');
  } catch (e) {
    console.log(e);
  }
});
app.set('files', './files');
app.set('view engine', 'ejs');
app.engine('ejs', engine);
client2.on('message', async (topic, msg) => {
  topic = topic.toString();
  var rest2 = topic.split("/");
  console.log('收到訊息:' + topic + msg.toString());
  var today = new Date();

  var currentDateTime =

    today.getFullYear() + '/' +

    (today.getMonth() + 1) + '/' +

    today.getDate() + '/(' +

    today.getHours() + ':' + today.getMinutes() +

    ')';
  for (var i = 1; i < 5; i++) {
    if (rest2[2] == i) {
      errors[i - 1][1].push(msg.toString());
      errors[i - 1][0].push(currentDateTime.toString());
    }
  }
});
app.get('/:userid/error', function (req, res) {

  var e = errors[parseInt(req.params.userid) - 1][1]
  var t = errors[parseInt(req.params.userid) - 1][0]
  res.render('errorlog', { // 這邊不用寫 views/index 是因為 express 預設 template 就是會放在 views 資料夾裡面
    er: e,
    time: t
    //items: ['peter', 'nick', 'cake']
  })
});
app.get('/agan', function (req, res) {
  res.render('agan')
})
// connect to db
//console.log(service.db.url);
mongoose.connect(service.db.url, service.db.options).then(async () => {
}).catch((err) => {
  console.log('cannot connect to mongodb, make sure `mongod` is started');
});
  console.log('mongodb online! start listening to sensors...');

  // make sure db is connected before recieving
  client.on('message', async (topic, msg) => {
    //console.log('correct');
    // get date to specify storage info
    const d = new Date();
    const year = d.getFullYear();
    let month = d.getMonth() + 1;
    month = (month < 10 ? '0' : '') + month;
    const date = d.getDate();
    const hour = d.getHours();
    // month = '10';
    // const date = 14;
    // const hour = 12;
    // extract topic & msg data
    try {
      const id = parseInt(topic.split('/')[2]);
      const data = JSON.parse(msg.toString());
      //console.log(typeof parseInt(data.tmp));
      try {
        await Sensor.update(
          { _id: `${id}:${year}${month}` },
          {
            $set: {
              [`sensors.${date}.${hour}.id_num`]: data.idn,
              [`sensors.${date}.${hour}.humidity`]: data.hum,
              [`sensors.${date}.${hour}.temperature`]: data.tmp,
              [`sensors.${date}.${hour}.soil_moisture1`]: data.smoi1,
              [`sensors.${date}.${hour}.soil_moisture2`]: data.smoi2,
              [`sensors.${date}.${hour}.soil_moisture3`]: data.smoi3,
              [`sensors.${date}.${hour}.soil_moisture4`]: data.smoi4,
              [`sensors.${date}.${hour}.soil_temperature`]: data.stmp,
              [`sensors.${date}.${hour}.luminance`]: data.lum,
              [`sensors.${date}.${hour}.ph`]: data.ph,
              [`sensors.${date}.${hour}.ec`]: data.ec,

            },
            $inc: {
              'count.id_num': 1,
              'count.humidity': 1,
              'count.temperature': 1,
              'count.soil_moisture1': 1,
              'count.soil_moisture2': 1,
              'count.soil_moisture3': 1,
              'count.soil_moisture4': 1,
              'count.soil_temperature': 1,
              'count.luminance': 1,
              'count.ph': 1,
              'count.ec': 1,

              // summed values of sensors
              'total.id_num': data.idn,
              'total.humidity': data.hum,
              'total.temperature': data.tmp,
              'total.soil_moisture1': data.smoi1,
              'total.soil_moisture2': data.smoi2,
              'total.soil_moisture3': data.smoi3,
              'total.soil_moisture4': data.smoi4,
              'total.soil_temperature': data.stmp,
              'total.luminance': data.lum,
              'total.ph': data.ph,
              'total.ec': data.ec,
            }
          },
          { upsert: true }
        );
      } catch (e) {
        console.log(e);
      }
    }
    catch (error) {
      console.log(error);
    }

  });
  clientagan.on('message', async (topic, msg) => {
    console.log('agan!!!!!!!');
    // get date to specify storage info
    const d = new Date();
    const year = d.getFullYear();
    let month = d.getMonth() + 1;
    month = (month < 10 ? '0' : '') + month;
    const date = d.getDate();
    const hour = d.getHours();
    // month = '10';
    // const date = 14;
    // const hour = 12;
    // extract topic & msg data
    const id = topic.split('/')[2];
    var data = "";
    console.log(id)
    try {
      console.log(msg.toString());
      data = JSON.parse(msg.toString());
      console.log(msg.toString());
      var co2data = 0;
      if (data.CO2 == undefined) {

      } else {
        co2data = data.CO2;
      }
      try {
        await aSensor.update(
          { _id: `${id}:${year}${month}` },
          {
            $set: {
              [`sensors.${date}.${hour}.id_num`]: data.idn,
              [`sensors.${date}.${hour}.humidity`]: data.hum,
              [`sensors.${date}.${hour}.temperature`]: data.tmp,
              [`sensors.${date}.${hour}.CH3CH2OH`]: data.CH3CH2OH,
              //  [`sensors.${date}.${hour}.o2`]: data.o2,
              [`sensors.${date}.${hour}.co2`]: co2data,
              //  [`sensors.${date}.${hour}.n2`]: data.n2,
              [`sensors.${date}.${hour}.status`]: data.st,
            },
            $inc: {
              'count.id_num': 1,
              'count.humidity': 1,
              'count.temperature': 1,
              'count.CH3CH2OH': 1,
              // 'count.o2': 1,
              'count.co2': 1,
              // 'count.n2': 1,
              'count.status': 1,

              // summed values of sensors
              'total.id_num': data.idn,
              'total.humidity': data.hum,
              'total.temperature': data.tmp,
              'total.CH3CH2OH': data.CH3CH2OH,
              // 'total.o2': data.o2,
              'total.co2': co2data,
              // 'total.n2': data.n2,
              'total.status': data.st,

            }
          },
          { upsert: true }
        );
      } catch (e) {
        console.log(e);
        return;
      }
    }
    catch (error) {
      console.log(error);
      return;
    }


  });
  clientncyu.on('message', async (topic, msg) => {
    console.log('ncyu!!!!!!!');
    // get date to specify storage info
    const d = new Date();
    const year = d.getFullYear();
    let month = d.getMonth() + 1;
    month = (month < 10 ? '0' : '') + month;
    const date = d.getDate();
    const hour = d.getHours();
    // month = '10';
    // const date = 14;
    // const hour = 12;
    // extract topic & msg data
    const id = topic.split('/')[2];
    console.log(id)
    try {
      const data = JSON.parse(msg.toString());
      console.log(typeof parseInt(data.tmp));
      try {
        await ncyuSensor.update(
          { _id: `${id}:${year}${month}` },
          {
            $set: {
              [`sensors.${date}.${hour}.id_num`]: data.idn,
              [`sensors.${date}.${hour}.humidity`]: data.hum,
              [`sensors.${date}.${hour}.temperature`]: data.tmp,
              [`sensors.${date}.${hour}.CH3CH2OH`]: data.CH3CH2OH,
              //  [`sensors.${date}.${hour}.o2`]: data.o2,
              // [`sensors.${date}.${hour}.co2`]: data.co2,
              // [`sensors.${date}.${hour}.n2`]: data.n2,
              [`sensors.${date}.${hour}.status`]: data.st,
            },
            $inc: {
              'count.id_num': 1,
              'count.humidity': 1,
              'count.temperature': 1,
              'count.CH3CH2OH': 1,
              //'count.o2': 1,
              // 'count.co2': 1,
              // 'count.n2': 1,
              'count.status': 1,

              // summed values of sensors
              'total.id_num': data.idn,
              'total.humidity': data.hum,
              'total.temperature': data.tmp,
              'total.CH3CH2OH': data.CH3CH2OH,
              // 'total.o2': data.o2,
              //  'total.co2': data.co2,
              //  'total.n2': data.n2,
              'total.status': data.st,

            }
          },
          { upsert: true }
        );
      } catch (e) {
        console.log(e);
        return;
      }
    }
    catch (error) {
      console.log(error);
      return;
    }

  });




module.exports = app;
