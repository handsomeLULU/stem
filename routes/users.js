const express = require('express');
const router = express.Router();
const passport = require('passport');
const mqtt = require('async-mqtt');

require('../config/passport').init(passport);
const auth = require('../config/passport').auth;
const User = require('../models/user');
const Site = require('../models/site');
const Sensor = require('../models/sensor');

router.post('/validate', async function (req, res) {
  const query = req.body;
  
  var user = [];
  try {
    user = await User.find(query);
  } catch (e) {
    console.log(e);
  } finally {
    const isUnique = user.length == 0 ? true : false;
    res.send(isUnique);
  }
});

// show all fields owned by user
router.get('/sites', auth(['ordinary']), async function (req, res) {
  let siteIds = [];
  // the query can only be accessed by supreme users
  if (Object.keys(req.query).includes('username') && req.user.role === 'supreme') {
    try {
      // get the site property by first eval the get
      siteIds = ( await User.findOne({ username: req.query.username }) ).site;
    } catch (e) {
      console.log(e);
    }
  } else {
    // if no query set id to current user
    siteIds = req.user.site;
  }

  let sites = [];
  try {
    // do NOT use findById because it only finds one entry, we want it all!!
    sites = await Site.find({ _id: siteIds }).select('location landNumber sensorId');
  } catch (e) {
    console.log(e);
  }
  
  res.json(sites);
});

// add new field for current user
router.post('/sites/add', auth(['ordinary']), async function (req,res) {
  const d = new Date();
  const year = d.getFullYear();
  const date = d.getDate(); // (1-31)
  let month = d.getMonth() + 1; // (0-11) -> (1-12)
  month = (month < 10 ? '0' : '') + month;
  let id0 = 0;
  try {
    // find the latest site so we can create new sensorId
    let latestSite = await Site.findOne().sort({ sensorId: -1 }).limit(1).select({ sensorId: 1 });
    
    // returns undefined on first entry
    if (latestSite == null) {
      id0 = 0;
    }
    else{
      id0 = latestSite.sensorId;
    }  
    // create a new site entry, with owner set as current user
    let siteInfo = req.body;
    // adding required info into siteInfo object
    siteInfo.owner = req.user._id;
    siteInfo.sensorId = id0 + 1;
    // create new entry
    const site = await Site.create(siteInfo);

    // create a new `data sheet`
    await Sensor.create({
      _id: `${site.sensorId}:${year}${month}`
    })
    // bind newly created site to current user
    await User.update({ _id: req.user._id }, { $push: { site: site._id }});

    // send response back to client
    res.json(site);
  } catch (e) {
    console.log(e);
  }
});

// delete sites
router.delete('/sites/delete', auth(['ordinary']), async function (req, res) {
  let site = null;
  
  try {
    site = await Site.findByIdAndDelete(req.body._id);
    await Sensor.deleteMany({ _id: {$regex: `^${site.sensorId}:` }});
    await User.update({ _id: req.user._id }, { $pull: { 'site': site._id }});
  } catch (e) {
    console.log(e);
  }
  res.send('ok');
});

///////////////////////////// admin routes ////////////////////////////////
// list users
router.get('/list', auth(['supreme']), async function (req, res) {
  let users = null;
  try {
    users = await User.find({}, { __v: 0 });
  } catch (e) {
    console.log(e);
  }

  res.json(users);
});

// delete individual user
router.delete('/delete', auth(['supreme']), async function (req, res) {
  const userId = req.body._id;
  // delete the required user and its assets
  try {
    // find distict values where owner contains userId
    const sid = await Site.find({ owner: userId }).distinct('sensorId');
    // build regex string from array
    const regex = sid.join('|');
    // delete all sensor data
    await Sensor.deleteMany({ _id: { $regex: `^${regex}:` }});
    await Site.deleteMany({ owner: userId });
    await User.deleteOne({ _id: userId });
  } catch (e) {
    console.log(e);
  }
  res.send('ok');
});

// toggle user role
router.post('/role/toggle', auth(['supreme']), async function (req, res) {
  let userId = req.body._id;
  
  // update user role
  let user = null;
  try {
    user = await User.findById(userId);
    let newRole = '';
    if (user.role === 'supreme') {
      newRole = 'ordinary';
    } else {
      newRole = 'supreme';
    }
    user = await User.findByIdAndUpdate(
      userId,
      { role: newRole },
      { new: true }
    );
  } catch (e) {
    console.log(e);
  }
  res.send(user);
});

module.exports = router;
