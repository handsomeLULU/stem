var passport = require('passport');
var jwt = require('jsonwebtoken');
var express = require('express');
var router = express.Router();

var service = require('../config/service');
var User = require('../models/user');
require('../config/passport').init(passport);

router.post('/register', async function (req, res) {
  if (!req.body.username || !req.body.password) {
    res.json({success: false, msg: 'username and password is required'});
  } else {
    // create user
    const user = new User(req.body);
    // save user
    try {
      await user.save();
    } catch (error) {
      console.log(error);
      
      return res.json({success: false, msg: 'username already exist'});
    }
    res.json({success: true, msg: 'user created'});
  }
});

router.post('/login', async function (req, res) {
  let user = {};
  // find user
  try {
    user = await User.findOne({ username: req.body.username });
    if (!user) {
      return res.status(401).send({ success: false, msg: 'user not found' });
      // return res.send({ success: false, msg: 'user not found' });
    }
  } catch (error) {
    console.log(error);
  }
  // compare password
  try {
    const isMatch = await user.comparePassword(req.body.password);
    if (isMatch) {
      filteredUser = user.toObject({ hide: 'password email __v _id site' });
      const token = jwt.sign(
        filteredUser,
        service.secret,
        // { expiresIn: 10080 }
      );
      // res.json({ success: true, token: 'JWT ' + token });
      res.json({ success: true, token: token, user: filteredUser });
    } else {
      res.status(401).send({ success: false, msg: 'wrong password' });
    }
  } catch (error) {
    console.log(error);
  }
});

module.exports = router;
