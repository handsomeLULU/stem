const express = require('express');
const router = express.Router();
const passport = require('passport');
require('../config/passport').init(passport);
const auth = require('../config/passport').auth;
const mongoose = require('mongoose');
const zusers = require('../models/userlist');
const zerrors = require('../models/erroragan');
const errorlog = require('../models/error');
const acts = require('../models/act');
const service = require('../config/service');
const Announcement = require('../models/announcement');
var errors = [];
var aganerrors = [];
var engine = require('ejs-locals');
for (var i = 0; i < 100; i++) {
  errors.push([[], []]);
}
for (var i = 0; i < 100; i++) {
  aganerrors.push([[], []]);
}

router.use(express.static('views'));
router.get('/0629/:userid/:errorlog', async function (req, res) {
  var today = new Date();
  var hour = today.getHours()
  if (hour > 24 || hour == 24) { hour = hour - 24; }
  var currentDateTime =
    today.getFullYear() + '/' +

    (today.getMonth() + 1) + '/' +

    today.getDate() + '/(' +

    hour + ':' + today.getMinutes() +

    ')';
 
  try {
    var errorlogs = new errorlog({
      number:req.params.userid,
      time: currentDateTime,
      data: req.params.errorlog.toString()
    });
    errorlogs.save(function (err) {
      if (err) {
        console.log(err);
      }
      console.log('成功');
    });
    res.send("ok");
  } catch (e) {
    console.log(e);
  }
});
router.get('/0629/act/:userid/:act', async function (req, res) {
  var today = new Date();
  var hour = today.getHours()
  if (hour > 24 || hour == 24) { hour = hour - 24; }
  var currentDateTime =
    today.getFullYear() + '/' +

    (today.getMonth() + 1) + '/' +

    today.getDate() + '/(' +

    hour + ':' + today.getMinutes() +

    ')';
  try {
    var act = new acts({
      number:req.params.userid,
      time: currentDateTime,
      data: req.params.act.toString()
    });
    act.save(function (err) {
      if (err) {
        console.log(err);
      }
      console.log('成功');
    });
    res.send("ok");
  } catch (e) {
    console.log(e);
  }

});
router.get('/0629/agan/:userid/:errorlog', async function (req, res) {
  var msg = req.params.errorlog.toString();
  var i = parseInt(req.params.userid);
  console.log(i + '收到訊息:' + msg.toString());
  var today = new Date();
  var hour = today.getHours()
  if (hour > 24 || hour == 24) { hour = hour - 24; }
  var currentDateTime =
    today.getFullYear() + '/' +

    (today.getMonth() + 1) + '/' +

    today.getDate() + '/(' +

    hour + ':' + today.getMinutes() +

    ')';
  aganerrors[i - 1][1].push(msg.toString());
  aganerrors[i - 1][0].push(currentDateTime.toString());
  //console.log(errors); 
  
    try {
      var error = new zerrors({
        number:req.params.userid,
        time: currentDateTime,
        data: req.params.errorlog.toString()
      });
      error.save(function (err) {
        if (err) {
          console.log(err);
        }
        console.log('成功');
      });
      res.send("ok");
    } catch (e) {
      console.log(e);
    }
  });
 // mongoose.disconnect();
  //res.send('200');


router.get('/:userid/act', async function (req, res) {
  acts.find({ number: req.params.userid }, function (err, e) {
    if (err) return handleError(err);
    var obj =e;
    var len =obj.length
    var data=[];
    for(var a=0;a<len-1;a++){
      var sitepar=obj[a].data.toString();
      var act = sitepar.slice(6);
      var flow= act[0]+act[1]+act[2]+act[3];
      if(flow=="flow"){
        var insertdata=obj[a].time+","+sitepar[4]+","+act;
      }else{
        var insertdata=obj[a].time+","+sitepar[4]+","+actransfer(act);
      }
      
      console.log(insertdata);
      data.push(insertdata);
    }
    //console.log(da);
    res.send(JSON.stringify(data));
    // 'athletes' contains the list of athletes that match the criteria.
  })
  
});
router.get('/:userid/action', async function (req, res) {
  acts.find({ number: req.params.userid }, function (err, e) {
    if (err) return handleError(err);
    var obj =e;
    var len =obj.length
    var data=[];
    for(var a=0;a<len-1;a++){
      var sitepar=obj[a].data.toString();
      var act = sitepar.slice(6);
      var flow= act[0]+act[1]+act[2]+act[3];
      
      if(flow=="flow"){
        var flownum=act.slice(5);
        var insertdata="時間："+obj[a].time+" , 田區："+sitepar[4]+" , 流量計："+flownum;
      }else{
        var insertdata="時間："+obj[a].time+" , 田區："+sitepar[4]+" , 動作："+actransfer(act);
      }
      
      
      console.log(insertdata);
      data.push(insertdata);
    }
    //console.log(da);
    res.render('action',{act:data.reverse()});
    // 'athletes' contains the list of athletes that match the criteria.
  })
  
});
router.get('/:userid/error', async function (req, res) {
  errorlog.find({ number: req.params.userid }, function (err, e) {
    var obj =e;
    var len =obj.length
    var data=[];
    for(var a=0;a<len-1;a++){
      var insertdata="時間："+obj[a].time+" , 訊息："+obj[a].data;
      data.push(insertdata);
    }
    //console.log(da);
    res.render('error',{act:data.reverse()});
    // 'athletes' contains the list of athletes that match the criteria.
  })
  
});
router.get('/agan/:userid/error', async function (req, res) {
  zerrors.find({ number: req.params.userid }, function (err, e) {
    var obj =e;
    var len =obj.length
    var data=[];
    for(var a=0;a<len-1;a++){
      var insertdata="時間："+obj[a].time+" , 訊息："+obj[a].data;
      data.push(insertdata);
    }
    //console.log(da);
    res.render('action',{act:data.reverse()});
  })
});

router.get('/0629/:number/:name/:id/:site', async function (req, res) {

  
    try {
      var zuser2 = new zusers({


        number: req.params.number,
        id: req.params.id,
        name: req.params.name,
        site: req.params.site
      });
      zuser2.save(function (err) {
        if (err) {
          console.log(err);
        }
        console.log('成功');
      });
      res.send("ok");
    } catch (e) {
      console.log(e);
    }

});
router.get('/delete/:number', async function (req, res) {
  zusers.deleteMany({ number: req.params.number }, function (err) { });
  res.send("ok")
})
router.get('/look', async function (req, res) {
  //console.log(users.find({}));
  zusers.find({}, function (err, users) {
    if (err) return handleError(err);
    //console.log(users);
    res.send(users);

    // 'athletes' contains the list of athletes that match the criteria.
  })
});
router.get('/insertid/:id/:lineid', async function (req, res) {
  zusers.findByIdAndUpdate(
    { _id: req.params.id },
    { $push: { id: req.params.lineid } },
    function (err, result) {
      if (err) {
        res.send(err);
      } else {
        res.send(result);
      }
    }
  );


})
router.get('/insertsite/:id/:site', async function (req, res) {
  zusers.findByIdAndUpdate(
    { _id: req.params.id },
    { $push: { site: req.params.site } },
    function (err, result) {
      if (err) {
        res.send(err);
      } else {
        res.send(result);
      }
    }
  );
})
router.get('/deletesite/:id/:site', async function (req, res) {
  zusers.findByIdAndUpdate(
    { _id: req.params.id },
    { $pull: { site: req.params.site } },
    function (err, result) {
      if (err) {
        res.send(err);
      } else {
        res.send(result);
      }
    }
  );
})
router.get('/deleteid/:id/:lineid', async function (req, res) {
  zusers.findByIdAndUpdate(
    { _id: req.params.id },
    { $pull: { id: req.params.lineid } },
    function (err, result) {
      if (err) {
        res.send(err);
      } else {
        res.send(result);
      }
    }
  );
})

router.get('/find/:userid/:siterid', async function (req, res) {
  //console.log(users.find({}));
  zusers.find({}, function (err, users) {
    if (err) return handleError(err);
    //users=JSON.parse(users.toString());
    console.log(typeof (users[0]));
    // var user=JSON.parse(users[0].toString());
    try { res.send(users[parseInt(req.params.userid)].id[parseInt(req.params.siterid)]); }
    catch (err) {
      console.error(err);
    };

    // 'athletes' contains the list of athletes that match the criteria.
  })
});
/* GET home page. */
router.get('/home', async function (req, res) {
  try {
    const anncs = await Announcement.find({}).sort({ date: -1 });
    res.json(anncs);
  } catch (e) {
    console.log(e);
  }
});
// add new announcement
router.post('/home/add', auth(['supreme']), async function (req, res) {
  let annc = null;
  try {
    annc = await Announcement.create(req.body);
  } catch (e) {
    console.log(e);
  }
  res.send(annc);
});

// edit announcement
router.post('/home/edit', auth(['supreme']), async function (req, res) {
  let annc = null;
  try {
    annc = await Announcement.findByIdAndUpdate(
      req.body._id,
      { title: req.body.title, body: req.body.body },
      { new: true }
    );
  } catch (e) {
    console.log(e);
  }
  res.send(annc);
});

// delete announcement
router.delete('/home', auth(['supreme']), async function (req, res) {
  try {
    await Announcement.findByIdAndDelete(req.body._id);
  } catch (e) {
    console.log(e);
  }
  res.send('announcement deleted');
});
function actransfer(action){
if(action=="watering"){
  return("供水")
}
else if(action=="massive_watering"){
  return("供水(三滴水)")
}
else if(action=="moderate_watering"){
  return("供水(兩滴水)")
}
else if(action=="less_watering"){
  return("供水(一滴水)")
}
else if(action=="custom_watering"){
  return("供水(自訂)")
}
else if(action=="cooling"){
  return("降溫")
}
else if(action=="high_cooling"){
  return("降溫(大太陽)")
}
else if(action=="normal_cooling"){
  return("降溫(很熱)")
}
else if(action=="low_cooling"){
  return("降溫(微熱)")
}
else if(action=="custom_cooling"){
  return("降溫(自訂)")
}
else if(action=="bugging"){
  return("除蟲")
}
else if(action=="fertiling"){
  return("施肥")
}
else if(action=="stop"){
  return("停止")
}
else if(action=="open"){
  return("開")
}
}
module.exports = router;
