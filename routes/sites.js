const express = require('express');
const router = express.Router();
const passport = require('passport');
require('../config/passport').init(passport);
const auth = require('../config/passport').auth;
const mqtt = require('async-mqtt');
const opts = require('../config/service').mqttOpts;

const Site = require('../models/site');
const Sensor = require('../models/sensor');


const aSite = require('../models/siteagan');
const aSensor = require('../models/sensoragan');
const ncyuSensor = require('../models/sensorncyu');
// get parameters of site
router.get('/:id/params', auth(['ordinary']), async function (req, res) {
  //router.get('/:id/params',async function (req, res) {  
  let site = null;
  console.log(req.params.id);
  try {
    site = await Site.findOne({ sensorId: req.params.id });
    //site = Site.findOne({ sensorId: req.params.id });
  } catch (e) {
    console.log(e);
  }
  res.json(site.params);
});

// edit and submit site params to edge device
router.post('/:id/params/edit', auth(['ordinary']), async function (req, res) {
  let site = null;
  try {
    site = await Site.findOneAndUpdate({
      $and: [{ owner: req.user._id }, { sensorId: req.params.id }]
    }, {
        $set: { 'params': req.body }
      }, { new: true });
  } catch (e) {
    console.log(e);
  }

  const paramStr = JSON.stringify(site.params);
  const client = mqtt.connect(opts);
  client.once('connect', async () => {
    try {
      await client.publish(`karotte/params/${req.params.id}`, paramStr);
      await client.end();
    } catch (e) {
      console.log(e);
    }
  });

  res.send('params saved and transmitted');

});

// get sensor control states
router.get('/:id/control', auth(['ordinary']), async function (req, res) {
  let site = null;
  try {
    site = await Site.findOne(
      { sensorId: +req.params.id }
    ).select(
      { water: 1, lights: 1, fan: 1, _id: 0 }
    );
  } catch (e) {
    console.log(e);
  }
  // returns a default of `false`, very convenient :)
  res.send(site);
});

// update manual controls
router.post('/:id/control', auth(['ordinary']), async function (req, res) {
  try {
    await Site.update({ sensorId: +req.params.id }, req.body);
  } catch (e) {
    console.log(e);
  }
  const ctrlStr = JSON.stringify(req.body);
  console.log(ctrlStr);

  const client = mqtt.connect(opts);
  client.once('connect', async () => {
    try {
      await client.publish(`karotte/controls/${req.params.id}`, ctrlStr);
      await client.end();
    } catch (e) {
      console.log(e);
    }
  });
  // res.send('ok');
});

// handles all sensor data query
// TODO: this is a bit messy
//router.get('/:id/data', auth(['ordinary']), async function (req, res) {
router.get('/:id/agan/data', async function (req, res) {
  const d = new Date();
  let year = d.getFullYear();
  let date = d.getDate(); // (1-31)
  let month = d.getMonth() + 1; // (0-11) -> (1-12)
  const monthStr = (month < 10 ? '0' : '') + month;
  const hour = d.getHours(); // (0-23)
  // define aggregation
  let aggregationSteps = [];
  let range = null; // will be init w/ query
  let labels = []; // ticks

  // construct aggregation pipeline according to query parameters
  if (Object.keys(req.query).length === 0) {
    console.log(`${req.params.id}:${year}${monthStr}`);
    // show current data if no query specified
    aggregationSteps = [{
      $match: { _id: `${req.params.id}:${year}${monthStr}` }
    }, {
      $replaceRoot: { newRoot: `$sensors.${date}.${hour}` }
    }];
  } else {
    // set default values in case shit blows up!!
    if (!Object.keys(req.query).includes('step')) {
      req.query.step = 'hour';
    }
    if (!Object.keys(req.query).includes('range')) {
      req.query.range = 6;
    }
    if (!Object.keys(req.query).includes('sensor')) {
      req.query.sensor = 'CH3CH2OH';
    }

    // queries are parsed in String format
    // so we need to transfer it to Number
    range = parseInt(req.query.range);

    // common aggregation steps
    aggregationSteps = [{
      $match: { _id: { $regex: `^${req.params.id}:(${year}|${year - 1})` } }
    }, {
      $project: {
        sensors: 1, total: 1, count: 1
      }
    }, {
      $sort: { _id: -1 }
    }];

    switch (req.query.step) {
      case 'month':
        // let m = parseInt(month)
        aggregationSteps = aggregationSteps.concat([{
          //   $match: { _id: { $regex: `^${req.params.id}:${year}` } }
          // }, {
          //   $project: {
          //     total: 1, count: 1
          //   }
          // }, {
          //   $addFields: {
          //     // get last two string of id (as month)
          //     time: {
          //       $substr: [
          //         '$_id',
          //         { $subtract: [{ $strLenCP: '$_id' }, 2] },
          //         2]
          //     }
          //   }
          // }, {
          $project: {
            _id: {
              // get the last 4 digits of _id
              $substr: ['$_id', { $subtract: [{ $strLenCP: '$_id' }, 4] }, -1]
            },
            id_num: { $divide: ['$total.id_num', '$count.id_num'] },
            humidity: { $divide: ['$total.humidity', '$count.humidity'] },
            temperature: { $divide: ['$total.temperature', '$count.temperature'] },
            // o2: { $divide: ['$total.o2', '$count.o2'] },
            CH3CH2OH: { $divide: ['$total.CH3CH2OH', '$count.CH3CH2OH'] },
            co2: { $divide: ['$total.co2', '$count.co2'] },
            // n2: { $divide: ['$total.n2', '$count.n2'] },
            status: { $divide: ['$total.status', '$count.status'] }

            //編號: { $divide: ['$total.編號', '$count.編號'] },
            //濕度: { $divide: ['$total.濕度', '$count.濕度'] },
            //溫度: { $divide: ['$total.溫度', '$count.溫度'] },
            //土壤濕度1: { $divide: ['$total.土壤濕度1', '$count.土壤濕度1'] },
            //土壤濕度2: { $divide: ['$total.土壤濕度2', '$count.土壤濕度2'] },
            //土壤濕度3: { $divide: ['$total.土壤濕度3', '$count.土壤濕度3'] },
            //土壤溫度: { $divide: ['$total.土壤溫度', '$count.土壤溫度'] },
            //光度: { $divide: ['$total.光度', '$count.光度'] },
            //酸鹼值: { $divide: ['$total.酸鹼值', '$count.酸鹼值'] },
            //ec值: { $divide: ['$total.ec值', '$count.ec值'] },
          }
        }, {
          $limit: range
        }, {
          $sort: { _id: -1 }
        }]);
        // set label of months
        // for (let i = month - range + 1; i <= month; i++) {
        for (let i = month; i > month - range; i--) {
          if (i <= 0) {
            labels.push(12 + i);
          } else {
            labels.push(i);
          }
        }

        break;

      case 'day':
        aggregationSteps = aggregationSteps.concat([{
          //   $match: { _id: { $regex: `^${req.params.id}:${year}(${month}|${month - 1})` } }
          // }, {
          $project: {
            sensors: { $objectToArray: `$sensors` },
          }
        }, {
          $unwind: '$sensors'
        }, {
          $addFields: {
            time: { $toInt: '$sensors.k' }
          }
        }, {
          $sort: { _id: -1, time: -1 }
        }, {
          $limit: range
        }, {
          $project: {
            _id: {
              $cond: {
                if: { $lt: [{ $toInt: '$sensors.k' }, 10] },
                then: { $concat: ['$_id', '0', '$sensors.k'] },
                else: { $concat: ['$_id', '$sensors.k'] }
              }
            },
            sensors: { $objectToArray: '$sensors.v' }
          }
        }, {
          $unwind: '$sensors'
        }, {
          $group: {
            _id: '$_id',
            'id_num': { $avg: '$sensors.v.id_num' },
            'humidity': { $avg: '$sensors.v.humidity' },
            'temperature': { $avg: '$sensors.v.temperature' },
            // 'o2': { $avg: '$sensors.v.o2' },
            'CH3CH2OH': { $avg: '$sensors.v.CH3CH2OH' },
            'co2': { $avg: '$sensors.v.co2' },
            // 'n2': { $avg: '$sensors.v.n2' },
            'status': { $avg: '$sensors.v.status' }

            //'編號': { $avg: '$sensors.v.編號' },
            //'濕度': { $avg: '$sensors.v.濕度' },
            //'溫度': { $avg: '$sensors.v.溫度' },
            //'土壤濕度1': { $avg: '$sensors.v.土壤濕度1' },
            //'土壤濕度2': { $avg: '$sensors.v.土壤濕度2' },
            //'土壤濕度3': { $avg: '$sensors.v.土壤濕度3' },
            // '土壤溫度': { $avg: '$sensors.v.土壤溫度' },
            //'光度': { $avg: '$sensors.v.光度' },
            //'酸鹼值': { $avg: '$sensors.v.酸鹼值' },
            //'ec值': { $avg: '$sensors.v.ec值' },
          }
        }, {
          $sort: { _id: -1 }
        }, {
          $project: {
            _id: { $substr: ['$_id', { $subtract: [{ $strLenCP: '$_id' }, 4] }, -1] },
            id_num: 1,
            humidity: 1,
            temperature: 1,
            CH3CH2OH: 1,
            //   o2: 1,
            co2: 1,
            //   n2: 1,
            status: 1

            //編號: 1,
            //濕度: 1,
            //溫度: 1,
            //土壤濕度1: 1,
            //土壤濕度2: 1,
            //土壤濕度3: 1,
            //土壤溫度: 1,
            //光度: 1,
            //酸鹼值: 1,
            //ec值: 1,
          }
        }]);
        // set label of date
        const lastDay = new Date(year, month, 0).getDate();
        for (let i = date; i > date - range; i--) {
          if (i <= 0) {
            labels.push(lastDay + i);
          } else {
            labels.push(i);
          }
        }

        break;

      case 'hour':
        aggregationSteps = aggregationSteps.concat([{
          //   $match: { _id: { $regex: `^${req.params.id}:${year}${month}` }}
          // }, {
          $project: {
            sensors: { $objectToArray: '$sensors' },
          }
        }, {
          $unwind: '$sensors'
        }, {
          $sort: { _id: -1, 'sensors.k': -1 }
        }, {
          $addFields: {
            time: {
              $cond: {
                if: { $lt: [{ $toInt: '$sensors.k' }, 10] },
                then: { $concat: ['0', '$sensors.k'] },
                else: { $concat: ['$sensors.k'] }
              }
            }
          }
        }, {
          $project: {
            _id: { $concat: ['$_id', '$time'] },
            sensors: { $objectToArray: '$sensors.v' },
            time: 1
          }
        }, {
          $unwind: '$sensors'
        }, {
          $project: {
            _id: 1,
            sensors: 1,
            time: { $toInt: '$sensors.k' }
          }
        }, {
          $sort: { _id: -1, 'time': -1 }
        }, {
          $limit: range
        }, {
          $project: {
            _id: {
              $concat: ['$_id', {
                $cond: {
                  if: { $lt: [{ $toInt: '$sensors.k' }, 10] },
                  then: { $concat: ['0', '$sensors.k'] },
                  else: { $concat: ['$sensors.k'] }
                }
              }]
            },
            id_num: '$sensors.v.id_num',
            humidity: '$sensors.v.humidity',
            temperature: '$sensors.v.temperature',
            CH3CH2OH: '$sensors.v.CH3CH2OH',
            //o2: '$sensors.v.o2',
            co2: '$sensors.v.co2',
            //n2: '$sensors.v.n2',
            status: '$sensors.v.status'
          }
        }, {
          $sort: { _id: -1 }
        }, {
          $project: {
            _id: {
              // get the last 4 digits of _id
              $substr: ['$_id', { $subtract: [{ $strLenCP: '$_id' }, 4] }, -1]
            },
            id_num: 1,
            humidity: 1,
            temperature: 1,
            CH3CH2OH: 1,
            //o2: 1,
            co2: 1,
            //n2: 1,
            status: 1

            ///編號: 1,
            //濕度: 1,
            //溫度: 1,
            //土壤濕度1: 1,
            //土壤濕度2: 1,
            //土壤濕度3: 1,
            //土壤溫度: 1,
            //光度: 1,
            // 酸鹼值: 1,
            // ec值: 1,
          }
        }]);
        // set label for hours
        for (let i = hour; i > hour - range; i--) {
          if (i < 0) {
            labels.push(24 + i);
          } else {
            labels.push(i);
          }
        }

        break;

      default:
        // no default behavior
        break;
    }
  }

  let sensorData = [];
  try {
    sensorData = await aSensor.aggregate(aggregationSteps);
    //sensorData = Sensor.aggregate(aggregationSteps); 
  } catch (e) {
    console.log(e);
  }

  // send a structured null object to frontend if there is no mathing data
  if (sensorData.length === 0) {
    sensorData[0] = {
      id_num: null,
      humidity: null,
      temperature: null,
      CH3CH2OH: null,
      //o2: null,
      co2: null,
      //n2: null,
      status: null,

    }
  } else if (sensorData.length > 1) {
    if (req.params.id == '1A') {
      for (s in sensorData) {
        //console.log(s);
        sensorData[s].temperature = sensorData[s].temperature - 8;
        sensorData[s].humidity = limitfunc(sensorData[s].humidity + 25);
        //console.log(sensorData[s].soil_moisture1);
      }
    }
    else if (req.params.id == '1B') {
      for (s in sensorData) {
        //console.log(s);
        sensorData[s].temperature = sensorData[s].temperature -4;
        sensorData[s].humidity = limitfunc(sensorData[s].humidity + 33);
        //console.log(sensorData[s].soil_moisture1);
      }
    }
    else if (req.params.id == '1C') {
      for (s in sensorData) {
        //console.log(s);
        sensorData[s].temperature = sensorData[s].temperature -3;
        sensorData[s].humidity = limitfunc(sensorData[s].humidity );
        //console.log(sensorData[s].soil_moisture1);
      }
    }
    else if (req.params.id == '1D') {
      for (s in sensorData) {
        //console.log(s);
        sensorData[s].temperature = sensorData[s].temperature - 3;
        sensorData[s].humidity = limitfunc(sensorData[s].humidity);
        //console.log(sensorData[s].soil_moisture1);
      }
    }
    else if (req.params.id == '1E') {
      for (s in sensorData) {
        //console.log(s);
        sensorData[s].temperature = sensorData[s].temperature -5;
        sensorData[s].humidity = limitfunc(sensorData[s].humidity-10 );
        //console.log(sensorData[s].soil_moisture1);
      }
    }
    else if (req.params.id == '1F') {
      for (s in sensorData) {
        //console.log(s);
        sensorData[s].temperature = sensorData[s].temperature - 9;
        sensorData[s].humidity = limitfunc(sensorData[s].humidity +12);
        //console.log(sensorData[s].soil_moisture1);
      }
    }
    else if (req.params.id == '1G') {
      for (s in sensorData) {
        //console.log(s);
        sensorData[s].temperature = sensorData[s].temperature -5;
        sensorData[s].humidity = limitfunc(sensorData[s].humidity );
        //console.log(sensorData[s].soil_moisture1);
      }
    }
    else if (req.params.id == '1H') {
      for (s in sensorData) {
        //console.log(s);
        sensorData[s].temperature = sensorData[s].temperature -4;
        sensorData[s].humidity = limitfunc(sensorData[s].humidity );
        //console.log(sensorData[s].soil_moisture1);
      }
    }
    else if (req.params.id == '2A') {
      for (s in sensorData) {
        //console.log(s);
        sensorData[s].temperature = sensorData[s].temperature - 5;
        sensorData[s].humidity = limitfunc(sensorData[s].humidity );
        //console.log(sensorData[s].soil_moisture1);
      }
    }
    else if (req.params.id == '2B') {
      for (s in sensorData) {
        //console.log(s);
        sensorData[s].temperature = sensorData[s].temperature - 6;
        sensorData[s].humidity = limitfunc(sensorData[s].humidity + 21);
        //console.log(sensorData[s].soil_moisture1);
      }
    }
    else if (req.params.id == '2C') {
      for (s in sensorData) {
        //console.log(s);
        sensorData[s].temperature = sensorData[s].temperature - 7;
        sensorData[s].humidity = limitfunc(sensorData[s].humidity+40 );
        //console.log(sensorData[s].soil_moisture1);
      }
    }
    else if (req.params.id == '2D') {
      for (s in sensorData) {
        //console.log(s);
        sensorData[s].temperature = sensorData[s].temperature - 4;
        sensorData[s].humidity = limitfunc(sensorData[s].humidity );
        //console.log(sensorData[s].soil_moisture1);
      }
    }
    else if (req.params.id == '2E') {
      for (s in sensorData) {
        //console.log(s);
        sensorData[s].temperature = sensorData[s].temperature - 5;
        sensorData[s].humidity = limitfunc(sensorData[s].humidity -5);
        //console.log(sensorData[s].soil_moisture1);
      }
    }
    else if (req.params.id == '2F') {
      for (s in sensorData) {
        //console.log(s);
        sensorData[s].temperature = sensorData[s].temperature - 9;
        sensorData[s].humidity = limitfunc(sensorData[s].humidity +16);
        //console.log(sensorData[s].soil_moisture1);
      }
    }
    else if (req.params.id == '2G') {
      for (s in sensorData) {
        //console.log(s);
        sensorData[s].temperature = sensorData[s].temperature-6;
        sensorData[s].humidity = limitfunc(sensorData[s].humidity+19);
        //console.log(sensorData[s].soil_moisture1);
      }
    }
    else if (req.params.id == '2H') {
      for (s in sensorData) {
        //console.log(s);
        sensorData[s].temperature = sensorData[s].temperature -4;
        sensorData[s].humidity = limitfunc(sensorData[s].humidity );
        //console.log(sensorData[s].soil_moisture1);
      }
    }
    else if (req.params.id == '3A') {
      for (s in sensorData) {
        //console.log(s);
        sensorData[s].temperature = sensorData[s].temperature - 2;
        sensorData[s].humidity = limitfunc(sensorData[s].humidity );
        //console.log(sensorData[s].soil_moisture1);
      }
    }
    else if (req.params.id == '3B') {
      for (s in sensorData) {
        //console.log(s);
        sensorData[s].temperature = sensorData[s].temperature - 2;
        sensorData[s].humidity = limitfunc(sensorData[s].humidity );
        //console.log(sensorData[s].soil_moisture1);
      }
    }
    else if (req.params.id == '4A') {
      for (s in sensorData) {
        //console.log(s);
        sensorData[s].temperature = sensorData[s].temperature - 7;
        sensorData[s].humidity = limitfunc(sensorData[s].humidity );
        //console.log(sensorData[s].soil_moisture1);
      }
    }
    else if (req.params.id == '4B') {
      for (s in sensorData) {
        //console.log(s);
        sensorData[s].temperature = sensorData[s].temperature - 4;
        sensorData[s].humidity = limitfunc(sensorData[s].humidity );
        //console.log(sensorData[s].soil_moisture1);
      }
    }
    else if (req.params.id == '5A') {
      for (s in sensorData) {
        //console.log(s);
        sensorData[s].temperature = sensorData[s].temperature - 4;
        //console.log(sensorData[s].soil_moisture1);
      }
    }
    // transform data 2 required format
    sensorData = db2dataset(sensorData, labels, req.query.sensor, req.query.step);
  }

  res.json(sensorData);
  //console.log(sensorData);
});
router.get('/:id/ncyu/data', async function (req, res) {
  const d = new Date();
  let year = d.getFullYear();
  let date = d.getDate(); // (1-31)
  let month = d.getMonth() + 1; // (0-11) -> (1-12)
  const monthStr = (month < 10 ? '0' : '') + month;
  const hour = d.getHours(); // (0-23)

  // define aggregation
  let aggregationSteps = [];
  let range = null; // will be init w/ query
  let labels = []; // ticks

  // construct aggregation pipeline according to query parameters
  if (Object.keys(req.query).length === 0) {
    console.log(`${req.params.id}:${year}${monthStr}`);
    // show current data if no query specified
    aggregationSteps = [{
      $match: { _id: `${req.params.id}:${year}${monthStr}` }
    }, {
      $replaceRoot: { newRoot: `$sensors.${date}.${hour}` }
    }];
  } else {
    // set default values in case shit blows up!!
    if (!Object.keys(req.query).includes('step')) {
      req.query.step = 'hour';
    }
    if (!Object.keys(req.query).includes('range')) {
      req.query.range = 6;
    }
    if (!Object.keys(req.query).includes('sensor')) {
      req.query.sensor = 'co2';
    }

    // queries are parsed in String format
    // so we need to transfer it to Number
    range = parseInt(req.query.range);

    // common aggregation steps
    aggregationSteps = [{
      $match: { _id: { $regex: `^${req.params.id}:(${year}|${year - 1})` } }
    }, {
      $project: {
        sensors: 1, total: 1, count: 1
      }
    }, {
      $sort: { _id: -1 }
    }];

    switch (req.query.step) {
      case 'month':
        // let m = parseInt(month)
        aggregationSteps = aggregationSteps.concat([{
          //   $match: { _id: { $regex: `^${req.params.id}:${year}` } }
          // }, {
          //   $project: {
          //     total: 1, count: 1
          //   }
          // }, {
          //   $addFields: {
          //     // get last two string of id (as month)
          //     time: {
          //       $substr: [
          //         '$_id',
          //         { $subtract: [{ $strLenCP: '$_id' }, 2] },
          //         2]
          //     }
          //   }
          // }, {
          $project: {
            _id: {
              // get the last 4 digits of _id
              $substr: ['$_id', { $subtract: [{ $strLenCP: '$_id' }, 4] }, -1]
            },
            id_num: { $divide: ['$total.id_num', '$count.id_num'] },
            humidity: { $divide: ['$total.humidity', '$count.humidity'] },
            temperature: { $divide: ['$total.temperature', '$count.temperature'] },
            o2: { $divide: ['$total.o2', '$count.o2'] },
            co2: { $divide: ['$total.co2', '$count.co2'] },
            n2: { $divide: ['$total.n2', '$count.n2'] },
            status: { $divide: ['$total.status', '$count.status'] }

            //編號: { $divide: ['$total.編號', '$count.編號'] },
            //濕度: { $divide: ['$total.濕度', '$count.濕度'] },
            //溫度: { $divide: ['$total.溫度', '$count.溫度'] },
            //土壤濕度1: { $divide: ['$total.土壤濕度1', '$count.土壤濕度1'] },
            //土壤濕度2: { $divide: ['$total.土壤濕度2', '$count.土壤濕度2'] },
            //土壤濕度3: { $divide: ['$total.土壤濕度3', '$count.土壤濕度3'] },
            //土壤溫度: { $divide: ['$total.土壤溫度', '$count.土壤溫度'] },
            //光度: { $divide: ['$total.光度', '$count.光度'] },
            //酸鹼值: { $divide: ['$total.酸鹼值', '$count.酸鹼值'] },
            //ec值: { $divide: ['$total.ec值', '$count.ec值'] },
          }
        }, {
          $limit: range
        }, {
          $sort: { _id: -1 }
        }]);
        // set label of months
        // for (let i = month - range + 1; i <= month; i++) {
        for (let i = month; i > month - range; i--) {
          if (i <= 0) {
            labels.push(12 + i);
          } else {
            labels.push(i);
          }
        }

        break;

      case 'day':
        aggregationSteps = aggregationSteps.concat([{
          //   $match: { _id: { $regex: `^${req.params.id}:${year}(${month}|${month - 1})` } }
          // }, {
          $project: {
            sensors: { $objectToArray: `$sensors` },
          }
        }, {
          $unwind: '$sensors'
        }, {
          $addFields: {
            time: { $toInt: '$sensors.k' }
          }
        }, {
          $sort: { _id: -1, time: -1 }
        }, {
          $limit: range
        }, {
          $project: {
            _id: {
              $cond: {
                if: { $lt: [{ $toInt: '$sensors.k' }, 10] },
                then: { $concat: ['$_id', '0', '$sensors.k'] },
                else: { $concat: ['$_id', '$sensors.k'] }
              }
            },
            sensors: { $objectToArray: '$sensors.v' }
          }
        }, {
          $unwind: '$sensors'
        }, {
          $group: {
            _id: '$_id',
            'id_num': { $avg: '$sensors.v.id_num' },
            'humidity': { $avg: '$sensors.v.humidity' },
            'temperature': { $avg: '$sensors.v.temperature' },
            'o2': { $avg: '$sensors.v.o2' },
            'co2': { $avg: '$sensors.v.co2' },
            'n2': { $avg: '$sensors.v.n2' },
            'status': { $avg: '$sensors.v.status' }

            //'編號': { $avg: '$sensors.v.編號' },
            //'濕度': { $avg: '$sensors.v.濕度' },
            //'溫度': { $avg: '$sensors.v.溫度' },
            //'土壤濕度1': { $avg: '$sensors.v.土壤濕度1' },
            //'土壤濕度2': { $avg: '$sensors.v.土壤濕度2' },
            //'土壤濕度3': { $avg: '$sensors.v.土壤濕度3' },
            // '土壤溫度': { $avg: '$sensors.v.土壤溫度' },
            //'光度': { $avg: '$sensors.v.光度' },
            //'酸鹼值': { $avg: '$sensors.v.酸鹼值' },
            //'ec值': { $avg: '$sensors.v.ec值' },
          }
        }, {
          $sort: { _id: -1 }
        }, {
          $project: {
            _id: { $substr: ['$_id', { $subtract: [{ $strLenCP: '$_id' }, 4] }, -1] },
            id_num: 1,
            humidity: 1,
            temperature: 1,
            o2: 1,
            co2: 1,
            n2: 1,
            status: 1

            //編號: 1,
            //濕度: 1,
            //溫度: 1,
            //土壤濕度1: 1,
            //土壤濕度2: 1,
            //土壤濕度3: 1,
            //土壤溫度: 1,
            //光度: 1,
            //酸鹼值: 1,
            //ec值: 1,
          }
        }]);
        // set label of date
        const lastDay = new Date(year, month, 0).getDate();
        for (let i = date; i > date - range; i--) {
          if (i <= 0) {
            labels.push(lastDay + i);
          } else {
            labels.push(i);
          }
        }

        break;

      case 'hour':
        aggregationSteps = aggregationSteps.concat([{
          //   $match: { _id: { $regex: `^${req.params.id}:${year}${month}` }}
          // }, {
          $project: {
            sensors: { $objectToArray: '$sensors' },
          }
        }, {
          $unwind: '$sensors'
        }, {
          $sort: { _id: -1, 'sensors.k': -1 }
        }, {
          $addFields: {
            time: {
              $cond: {
                if: { $lt: [{ $toInt: '$sensors.k' }, 10] },
                then: { $concat: ['0', '$sensors.k'] },
                else: { $concat: ['$sensors.k'] }
              }
            }
          }
        }, {
          $project: {
            _id: { $concat: ['$_id', '$time'] },
            sensors: { $objectToArray: '$sensors.v' },
            time: 1
          }
        }, {
          $unwind: '$sensors'
        }, {
          $project: {
            _id: 1,
            sensors: 1,
            time: { $toInt: '$sensors.k' }
          }
        }, {
          $sort: { _id: -1, 'time': -1 }
        }, {
          $limit: range
        }, {
          $project: {
            _id: {
              $concat: ['$_id', {
                $cond: {
                  if: { $lt: [{ $toInt: '$sensors.k' }, 10] },
                  then: { $concat: ['0', '$sensors.k'] },
                  else: { $concat: ['$sensors.k'] }
                }
              }]
            },
            id_num: '$sensors.v.id_num',
            humidity: '$sensors.v.humidity',
            temperature: '$sensors.v.temperature',
            o2: '$sensors.v.o2',
            co2: '$sensors.v.co2',
            n2: '$sensors.v.n2',
            status: '$sensors.v.status'
          }
        }, {
          $sort: { _id: -1 }
        }, {
          $project: {
            _id: {
              // get the last 4 digits of _id
              $substr: ['$_id', { $subtract: [{ $strLenCP: '$_id' }, 4] }, -1]
            },
            id_num: 1,
            humidity: 1,
            temperature: 1,
            o2: 1,
            co2: 1,
            n2: 1,
            status: 1

            ///編號: 1,
            //濕度: 1,
            //溫度: 1,
            //土壤濕度1: 1,
            //土壤濕度2: 1,
            //土壤濕度3: 1,
            //土壤溫度: 1,
            //光度: 1,
            // 酸鹼值: 1,
            // ec值: 1,
          }
        }]);
        // set label for hours
        for (let i = hour; i > hour - range; i--) {
          if (i < 0) {
            labels.push(24 + i);
          } else {
            labels.push(i);
          }
        }

        break;

      default:
        // no default behavior
        break;
    }
  }

  let sensorData = [];
  try {
    sensorData = await ncyuSensor.aggregate(aggregationSteps);
    //sensorData = Sensor.aggregate(aggregationSteps); 
  } catch (e) {
    console.log(e);
  }

  // send a structured null object to frontend if there is no mathing data
  if (sensorData.length === 0) {
    sensorData[0] = {
      id_num: null,
      humidity: null,
      temperature: null,
      o2: null,
      co2: null,
      n2: null,
      status: null,

    }
  } else if (sensorData.length > 1) {
    // transform data 2 required format
    sensorData = db2dataset(sensorData, labels, req.query.sensor, req.query.step);
  }

  res.json(sensorData);
  //console.log(sensorData);
});
router.get('/:id/data', async function (req, res) {
  const d = new Date();
  let year = d.getFullYear();
  let date = d.getDate(); // (1-31)
  let month = d.getMonth() + 1; // (0-11) -> (1-12)
  const monthStr = (month < 10 ? '0' : '') + month;
  const hour = d.getHours(); // (0-23)

  // define aggregation
  let aggregationSteps = [];
  let range = null; // will be init w/ query
  let labels = []; // ticks

  // construct aggregation pipeline according to query parameters
  if (Object.keys(req.query).length === 0) {
    console.log(`${req.params.id}:${year}${monthStr}`);
    // show current data if no query specified
    aggregationSteps = [{
      $match: { _id: `${req.params.id}:${year}${monthStr}` }
    }, {
      $replaceRoot: { newRoot: `$sensors.${date}.${hour}` }
    }];
  } else {
    // set default values in case shit blows up!!
    if (!Object.keys(req.query).includes('step')) {
      req.query.step = 'hour';
    }
    if (!Object.keys(req.query).includes('range')) {
      req.query.range = 6;
    }
    if (!Object.keys(req.query).includes('sensor')) {
      req.query.sensor = 'temperature';
    }

    // queries are parsed in String format
    // so we need to transfer it to Number
    range = parseInt(req.query.range);

    // common aggregation steps
    aggregationSteps = [{
      $match: { _id: { $regex: `^${req.params.id}:(${year}|${year - 1})` } }
    }, {
      $project: {
        sensors: 1, total: 1, count: 1
      }
    }, {
      $sort: { _id: -1 }
    }];

    switch (req.query.step) {
      case 'month':
        // let m = parseInt(month)
        aggregationSteps = aggregationSteps.concat([{
          //   $match: { _id: { $regex: `^${req.params.id}:${year}` } }
          // }, {
          //   $project: {
          //     total: 1, count: 1
          //   }
          // }, {
          //   $addFields: {
          //     // get last two string of id (as month)
          //     time: {
          //       $substr: [
          //         '$_id',
          //         { $subtract: [{ $strLenCP: '$_id' }, 2] },
          //         2]
          //     }
          //   }
          // }, {
          $project: {
            _id: {
              // get the last 4 digits of _id
              $substr: ['$_id', { $subtract: [{ $strLenCP: '$_id' }, 4] }, -1]
            },
            id_num: { $divide: ['$total.id_num', '$count.id_num'] },
            humidity: { $divide: ['$total.humidity', '$count.humidity'] },
            temperature: { $divide: ['$total.temperature', '$count.temperature'] },
            soil_moisture1: { $divide: ['$total.soil_moisture1', '$count.soil_moisture1'] },
            soil_moisture2: { $divide: ['$total.soil_moisture2', '$count.soil_moisture2'] },
            soil_moisture3: { $divide: ['$total.soil_moisture3', '$count.soil_moisture3'] },
            soil_moisture4: { $divide: ['$total.soil_moisture4', '$count.soil_moisture4'] },
            soil_temperature: { $divide: ['$total.soil_temperature', '$count.soil_temperature'] },
            luminance: { $divide: ['$total.luminance', '$count.luminance'] },
            ph: { $divide: ['$total.ph', '$count.ph'] },
            ec: { $divide: ['$total.ec', '$count.ec'] },

            //編號: { $divide: ['$total.編號', '$count.編號'] },
            //濕度: { $divide: ['$total.濕度', '$count.濕度'] },
            //溫度: { $divide: ['$total.溫度', '$count.溫度'] },
            //土壤濕度1: { $divide: ['$total.土壤濕度1', '$count.土壤濕度1'] },
            //土壤濕度2: { $divide: ['$total.土壤濕度2', '$count.土壤濕度2'] },
            //土壤濕度3: { $divide: ['$total.土壤濕度3', '$count.土壤濕度3'] },
            //土壤溫度: { $divide: ['$total.土壤溫度', '$count.土壤溫度'] },
            //光度: { $divide: ['$total.光度', '$count.光度'] },
            //酸鹼值: { $divide: ['$total.酸鹼值', '$count.酸鹼值'] },
            //ec值: { $divide: ['$total.ec值', '$count.ec值'] },
          }
        }, {
          $limit: range
        }, {
          $sort: { _id: -1 }
        }]);
        // set label of months
        // for (let i = month - range + 1; i <= month; i++) {
        for (let i = month; i > month - range; i--) {
          if (i <= 0) {
            labels.push(12 + i);
          } else {
            labels.push(i);
          }
        }

        break;

      case 'day':
        aggregationSteps = aggregationSteps.concat([{
          //   $match: { _id: { $regex: `^${req.params.id}:${year}(${month}|${month - 1})` } }
          // }, {
          $project: {
            sensors: { $objectToArray: `$sensors` },
          }
        }, {
          $unwind: '$sensors'
        }, {
          $addFields: {
            time: { $toInt: '$sensors.k' }
          }
        }, {
          $sort: { _id: -1, time: -1 }
        }, {
          $limit: range
        }, {
          $project: {
            _id: {
              $cond: {
                if: { $lt: [{ $toInt: '$sensors.k' }, 10] },
                then: { $concat: ['$_id', '0', '$sensors.k'] },
                else: { $concat: ['$_id', '$sensors.k'] }
              }
            },
            sensors: { $objectToArray: '$sensors.v' }
          }
        }, {
          $unwind: '$sensors'
        }, {
          $group: {
            _id: '$_id',
            'id_num': { $avg: '$sensors.v.id_num' },
            'humidity': { $avg: '$sensors.v.humidity' },
            'temperature': { $avg: '$sensors.v.temperature' },
            'soil_moisture1': { $avg: '$sensors.v.soil_moisture1' },
            'soil_moisture2': { $avg: '$sensors.v.soil_moisture2' },
            'soil_moisture3': { $avg: '$sensors.v.soil_moisture3' },
            'soil_moisture4': { $avg: '$sensors.v.soil_moisture4' },
            'soil_temperature': { $avg: '$sensors.v.soil_temperature' },
            'luminance': { $avg: '$sensors.v.luminance' },
            'ph': { $avg: '$sensors.v.ph' },
            'ec': { $avg: '$sensors.v.ec' },

            //'編號': { $avg: '$sensors.v.編號' },
            //'濕度': { $avg: '$sensors.v.濕度' },
            //'溫度': { $avg: '$sensors.v.溫度' },
            //'土壤濕度1': { $avg: '$sensors.v.土壤濕度1' },
            //'土壤濕度2': { $avg: '$sensors.v.土壤濕度2' },
            //'土壤濕度3': { $avg: '$sensors.v.土壤濕度3' },
            // '土壤溫度': { $avg: '$sensors.v.土壤溫度' },
            //'光度': { $avg: '$sensors.v.光度' },
            //'酸鹼值': { $avg: '$sensors.v.酸鹼值' },
            //'ec值': { $avg: '$sensors.v.ec值' },
          }
        }, {
          $sort: { _id: -1 }
        }, {
          $project: {
            _id: { $substr: ['$_id', { $subtract: [{ $strLenCP: '$_id' }, 4] }, -1] },
            id_num: 1,
            humidity: 1,
            temperature: 1,
            soil_moisture1: 1,
            soil_moisture2: 1,
            soil_moisture3: 1,
            soil_moisture4: 1,
            soil_temperature: 1,
            luminance: 1,
            ph: 1,
            ec: 1,

            //編號: 1,
            //濕度: 1,
            //溫度: 1,
            //土壤濕度1: 1,
            //土壤濕度2: 1,
            //土壤濕度3: 1,
            //土壤溫度: 1,
            //光度: 1,
            //酸鹼值: 1,
            //ec值: 1,
          }
        }]);
        // set label of date
        const lastDay = new Date(year, month, 0).getDate();
        for (let i = date; i > date - range; i--) {
          if (i <= 0) {
            labels.push(lastDay + i);
          } else {
            labels.push(i);
          }
        }

        break;

      case 'hour':
        aggregationSteps = aggregationSteps.concat([{
          //   $match: { _id: { $regex: `^${req.params.id}:${year}${month}` }}
          // }, {
          $project: {
            sensors: { $objectToArray: '$sensors' },
          }
        }, {
          $unwind: '$sensors'
        }, {
          $sort: { _id: -1, 'sensors.k': -1 }
        }, {
          $addFields: {
            time: {
              $cond: {
                if: { $lt: [{ $toInt: '$sensors.k' }, 10] },
                then: { $concat: ['0', '$sensors.k'] },
                else: { $concat: ['$sensors.k'] }
              }
            }
          }
        }, {
          $project: {
            _id: { $concat: ['$_id', '$time'] },
            sensors: { $objectToArray: '$sensors.v' },
            time: 1
          }
        }, {
          $unwind: '$sensors'
        }, {
          $project: {
            _id: 1,
            sensors: 1,
            time: { $toInt: '$sensors.k' }
          }
        }, {
          $sort: { _id: -1, 'time': -1 }
        }, {
          $limit: range
        }, {
          $project: {
            _id: {
              $concat: ['$_id', {
                $cond: {
                  if: { $lt: [{ $toInt: '$sensors.k' }, 10] },
                  then: { $concat: ['0', '$sensors.k'] },
                  else: { $concat: ['$sensors.k'] }
                }
              }]
            },
            id_num: '$sensors.v.id_num',
            humidity: '$sensors.v.humidity',
            temperature: '$sensors.v.temperature',
            soil_moisture1: '$sensors.v.soil_moisture1',
            soil_moisture2: '$sensors.v.soil_moisture2',
            soil_moisture3: '$sensors.v.soil_moisture3',
            soil_moisture4: '$sensors.v.soil_moisture4',
            soil_temperature: '$sensors.v.soil_temperature',
            luminance: '$sensors.v.luminance',
            ph: '$sensors.v.ph',
            ec: '$sensors.v.ec',
          }
        }, {
          $sort: { _id: -1 }
        }, {
          $project: {
            _id: {
              // get the last 4 digits of _id
              $substr: ['$_id', { $subtract: [{ $strLenCP: '$_id' }, 4] }, -1]
            },
            id_num: 1,
            humidity: 1,
            temperature: 1,
            soil_moisture1: 1,
            soil_moisture2: 1,
            soil_moisture3: 1,
            soil_moisture4: 1,
            soil_temperature: 1,
            luminance: 1,
            ph: 1,
            ec: 1,

            ///編號: 1,
            //濕度: 1,
            //溫度: 1,
            //土壤濕度1: 1,
            //土壤濕度2: 1,
            //土壤濕度3: 1,
            //土壤溫度: 1,
            //光度: 1,
            // 酸鹼值: 1,
            // ec值: 1,
          }
        }]);
        // set label for hours
        for (let i = hour; i > hour - range; i--) {
          if (i < 0) {
            labels.push(24 + i);
          } else {
            labels.push(i);
          }
        }

        break;

      default:
        // no default behavior
        break;
    }
  }

  let sensorData = [];
  try {
    sensorData = await Sensor.aggregate(aggregationSteps);
    //sensorData = Sensor.aggregate(aggregationSteps); 
  } catch (e) {
    console.log(e);
  }

  // send a structured null object to frontend if there is no mathing data
  if (sensorData.length === 0) {
    sensorData[0] = {
      id_num: null,
      humidity: null,
      temperature: null,
      soil_moisture1: null,
      soil_moisture2: null,
      soil_moisture3: null,
      soil_moisture4: null,
      soil_temperature: null,
      luminance: null,
      ph: null,
      ec: null,
    }
  } else if (sensorData.length > 1) {
    if (req.params.id == 13) {
      for (s in sensorData) {
        sensorData[s].soil_moisture1 = count(sensorData[s].soil_moisture1 +100);
        sensorData[s].soil_moisture2 = count(sensorData[s].soil_moisture2 +100);
        sensorData[s].soil_moisture3 = count(sensorData[s].soil_moisture3 +100);
      }
    }
    else if (req.params.id == 14) {
      for (s in sensorData) {
        sensorData[s].soil_moisture1 = count(sensorData[s].soil_moisture1 +100);
        sensorData[s].soil_moisture2 = count(sensorData[s].soil_moisture2 +100);
        sensorData[s].soil_moisture3 = count(sensorData[s].soil_moisture3 +100);
      }
    }
    else if (req.params.id == 15) {
      for (s in sensorData) {
        sensorData[s].soil_moisture1 = count(sensorData[s].soil_moisture1 );
        sensorData[s].soil_moisture2 = count(sensorData[s].soil_moisture2 );
        sensorData[s].soil_moisture3 = count(sensorData[s].soil_moisture3 );
      }
    }
    else if (req.params.id == 16) {
      for (s in sensorData) {
        sensorData[s].soil_moisture1 = count(sensorData[s].soil_moisture1 );
        sensorData[s].soil_moisture2 = count(sensorData[s].soil_moisture2 );
        sensorData[s].soil_moisture3 = count(sensorData[s].soil_moisture3 );
      }
    }
    // transform data 2 required format
    sensorData = db2dataset(sensorData, labels, req.query.sensor, req.query.step);
  }

  res.json(sensorData);
  //console.log(sensorData);
});
function count(num){
 return(limitfunc(100-num/8))
}
function limitfunc(num){
  if(num>100){
    return(100);
  }
  else if(num<0){
    return(0);
  }
  else{
    return(num);
  }
}
/////////////// helper functions ////////////////

// DB2DATASET is used to translate db structured data into chart.js dataset format
// this function is very important, since it has to cope w/ different senarios
// in hour mode `lbls` is represented as
// >> [16,17,18,19,20,21,22,23,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]
// it is easily seen that the starting index of the latest day is where the value is 0
// and in other modes the starting index of latest day/month is at 1
let db2dataset = (data, lbls, sname, mode) => {
  const d = new Date();
  let indexPos = 0;
  let currentPrimaryIdx = 0;
  let startIdx = 0;

  switch (mode) {
    case 'month':
      // indexPos = 1;
      currentPrimaryIdx = parseInt(d.getFullYear().toString().slice(-2));
      startIdx = 1;
      break;

    case 'day':
      // indexPos = 1;
      currentPrimaryIdx = d.getMonth() + 1;
      startIdx = 1;
      break;

    case 'hour':
      // indexPos = 0;
      currentPrimaryIdx = d.getDate();
      startIdx = 0;
      break;

    default:
      break;
  }

  let dset = Array(lbls.length);
  let primaryIdx = currentPrimaryIdx;
  let secondaryIdx = 0;
  // let indexPos = (mode === 'hour' ? 0 : 1);
  data.some(element => {

    const currentSecondaryIdx = +element._id.slice(2);

    // secondaryIdx = +element._id.slice(2);

    // starting index
    // if (primaryIdx === currentPrimaryIdx) {
    //   // it there are no day/month crossings there will be no 0 or 1 involved
    //   indexPos = lbls.indexOf(indexPos) === -1 ? 0 : lbls.indexOf(indexPos);
    //   console.log(indexPos);
    // }

    const idx = lbls.indexOf(currentSecondaryIdx, indexPos);
    dset[idx] = element[sname];

    indexPos = idx;

    primaryIdx = +element._id.slice(0, 2);
    // check if primaryIdx is same as current
    if (primaryIdx !== currentPrimaryIdx) {
      // console.log(currentPrimaryIdx, primaryIdx, currentSecondaryIdx, startIdx);
      return secondaryIdx !== startIdx;
    } else {
      secondaryIdx = currentSecondaryIdx;
      // console.log(indexPos);
    }

    // secondaryIdx = +element._id.slice(2);
  });

  const collection = {
    labels: lbls.reverse(),
    dataset: dset.reverse()
  };

  return collection;
}

module.exports = router;
