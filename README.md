# Karotte Stem
the backbone of our automatic agriculture app.

## Usage
to run it standalone, make sure your PWD is in the parent of the project folder
```
docker build -t api -f ./karotte-stem/Dockerfile
docker run --rm -it -p "3000:3000" api
```
make sure you have mongodb and mosquitto installed

## Development
go to the project directory and type
```
npm install
```
and then
```
npm start
```
*this project is created by microARC*