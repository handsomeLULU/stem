const DB_HOST = process.env.DB_HOST || 'localhost';
//const DB_HOST ='localhost';
const MQTT_BROKER = process.env.MQTT_BROKER || 'localhost';

module.exports = {
  secret: 'oh-my-holy-secret',
  db: {
    url: `mongodb://${DB_HOST}:27017/karotte`,
    options: { useNewUrlParser: true}
  },
  mqttOpts: {
    host: MQTT_BROKER,
    port: 1883,
    clientId: 'webapp'
  }
}
