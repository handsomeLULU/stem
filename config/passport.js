// const LocalStrategy = require('passport-local').Strategy;
const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
// const bcrypt = require('bcryptjs');
const User = require('../models/user');
const service = require('../config/service');

let passportInstance = null

module.exports = {
  // setting up JWT authentication
  init: (passport) => {
    passportInstance = passport;
    let opts = {};
    opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
    opts.secretOrKey = service.secret;
    passport.use(new JwtStrategy(opts, async function (jwtPayload, done) {
      // find user from db
      let user = false;
      try {
        user = await User.findOne({ username: jwtPayload.username });
      } catch (e) {
        return done(e, false);
      }

      done(null, user);
    }));
  },

  // returns a function that acts as the middleware
  auth: (roles) => {
    return (req, res, next) => {
      passportInstance.authenticate('jwt', { session: false }, function (err, user) {
        if (err) return next(err);
        if (!user) return res.sendStatus(401);

        // supreme user is always allowed
        if (!roles.includes('supreme')) {
          roles.push('supreme')
        }

        // check user role
        if (roles.includes(user.role)) {
          req.user = user;
          next();
        }

        if (Object.keys(req.user).length === 0) {
          res.status(403).send('you have no right');
        }
        
      })(req, res, next);
    }
  }
}