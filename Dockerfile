FROM node:8.12.0-alpine

# timezone setup
RUN apk update && apk upgrade && \
    apk add --update --no-cache tzdata
RUN ln -sf /usr/share/zoneinfo/Asia/Taipei /etc/localtime

# install deps
WORKDIR /usr/src/app
COPY karotte-stem/package*.json ./
RUN npm install --production

COPY karotte-stem .

EXPOSE 3000

CMD [ "npm", "start" ]